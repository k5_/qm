package eu.k5.am.fx.start

import eu.k5.am.fx.View
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox
import javax.annotation.PostConstruct
import javax.inject.Inject

class StartView @Inject constructor(val model: StartModel) : View {


	val openProject = TextField();

	val connectProject = TextField();

	val openProjectButton = Button("Open Project");

	val connectProjectButton = Button();

	@PostConstruct
	fun init() {
		openProject.textProperty().bind(model.openProject);

		openProjectButton.onAction = EventHandler<ActionEvent> { model.openProject() };
	}

	override fun display(pane: Pane) {
		val hbox = VBox();
		hbox.children.addAll(openProject, openProjectButton, connectProject, connectProjectButton);
		pane.children.add(hbox);

	}
}