package eu.k5.am.fx

import eu.k5.am.fx.start.StartView
import javafx.scene.Scene
import javafx.scene.layout.StackPane
import javafx.stage.Stage
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.spi.Bean
import javax.enterprise.inject.spi.CDI

@ApplicationScoped
class EntryPoint {

	private var primaryStage: Stage? = null;

	
	
	fun startup(stage: Stage) {
		primaryStage = stage;
		primaryStage!!.title = "Open Project";
		val start = resolveStart();
		val root = StackPane()

		start.display(root);
		stage.scene = Scene(root, 300.0, 300.0)
		stage.show()
	}

	fun resolveStart(): View {
		val bm = CDI.current().beanManager;
		val bean: Bean<View>? = bm.resolve(bm.getBeans(StartView::class.java)) as? Bean<View>?;

		if (bean != null) {
			val cc = bm.createCreationalContext(bean);
			if (cc != null) {
				return bean.create(cc);
			}
		}
		throw IllegalArgumentException("Unable to find Bean for StartView");
	}
}