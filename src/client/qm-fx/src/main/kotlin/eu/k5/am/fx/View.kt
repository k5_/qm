package eu.k5.am.fx

import javafx.scene.layout.Pane

interface View {

	fun display(pane: Pane)

}
