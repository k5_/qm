package eu.k5.am.fx

import javafx.application.Application
import javafx.stage.Stage
import org.jboss.weld.environment.se.StartMain
import org.jboss.weld.environment.se.bindings.Parameters
import org.jboss.weld.environment.se.events.ContainerInitialized
import javax.enterprise.event.Observes
import javax.enterprise.inject.spi.Bean
import javax.enterprise.inject.spi.CDI


fun main(args: Array<String>) {
	StartMain.main(args);
}


class Main {

	fun bootListener(@Observes event: ContainerInitialized, @Parameters args: List<String>) {
		Thread({ -> Application.launch(FxApp::class.java) }).start();
	}

}

class FxApp : Application() {
	override fun start(stage: Stage?) {
		if (stage == null){
			return;
		}
		val entry = resolveEntry();
	
		entry.startup(stage);
	}

	fun resolveEntry(): EntryPoint {
		val bm = CDI.current().beanManager;
		val bean: Bean<EntryPoint>? = bm.resolve(bm.getBeans(EntryPoint::class.java)) as? Bean<EntryPoint>?;

		if (bean != null) {
			val cc = bm.createCreationalContext(bean);
			if (cc != null) {
				return bean.create(cc);
			}
		}
		throw IllegalArgumentException("Unable to find Bean for StartView");
	}


}