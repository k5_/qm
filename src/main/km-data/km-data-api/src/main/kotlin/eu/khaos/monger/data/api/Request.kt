package eu.khaos.monger.data.api

class Request {

    var name: String? = null

    var headers: Map<String, String>? = HashMap()

    var content: String? = null

}