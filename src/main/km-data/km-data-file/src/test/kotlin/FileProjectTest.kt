import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import eu.khaos.monger.data.file.FileProject
import eu.khaos.monger.data.file.FileRequest
import org.junit.Test
import java.io.FileOutputStream
import java.nio.file.Paths
import javax.xml.bind.JAXBContext


class FileProjectTest {



	@Test
	fun exp(){

		val request = FileRequest()

		request.name = "name"
		request.content = "<project></project>"


		val mapper = ObjectMapper(YAMLFactory())
		mapper.writeValue(FileOutputStream("test.yaml"), request);

}

	@Test
	fun test(){

		val context = JAXBContext.newInstance(FileProject::class.java);
		val unmarshaller = context.createUnmarshaller();
		
		val obj = unmarshaller.unmarshal(Paths.get("src", "test", "resources", "projects", "basic", "project.xml").toFile());
	
		if (obj is FileProject){
			println(obj.name);
		}
		
	}

}