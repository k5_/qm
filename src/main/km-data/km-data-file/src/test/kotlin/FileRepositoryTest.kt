import eu.khaos.monger.data.api.ProjectNotFoundException
import eu.khaos.monger.data.api.Request
import eu.khaos.monger.data.api.RequestCreate
import eu.khaos.monger.data.file.FileRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Created by k5 on 27.06.2017.
 */


class FileRepositoryTest {

    private var repository: FileRepository? = null

    private var writeRepository: FileRepository? = null

    @Before fun init() {

        repository = FileRepository(Paths.get("src", "test", "resources", "repository"))

        val writePath = Paths.get("target", "repositories", "_" + System.currentTimeMillis())

        Files.createDirectories(writePath)

        writeRepository = FileRepository(writePath)

    }


    @Test(expected = ProjectNotFoundException::class) fun openProject__notFound() {
        repository!!.openProject("notfound")

    }

    @Test fun openProject() {
        val project = repository!!.openProject("basic")
        Assert.assertEquals("name", project!!.name)
    }

    @Test fun createProject() {
        val createdProject = writeRepository!!.createProject("exp")
        var newRequest = RequestCreate();
        newRequest.name = "request"
        writeRepository!!.createRequest(createdProject, newRequest)

    }

}