package eu.khaos.monger.data.file

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import eu.khaos.monger.data.api.*
import java.nio.file.Files
import java.nio.file.Path
import java.util.*


class FileRepository(val baseDirectory: Path) {


    fun openProject(name: String): Project? {
        val path = baseDirectory.resolve(name).resolve("project.xml")


        if (!Files.exists(path)) {
            throw ProjectNotFoundException()
        }
        val fileProject = Context.open(path)

        return map(fileProject)
    }


    fun createProject(name: String): Project {
        val projectPath = baseDirectory.resolve(name)
        if (Files.exists(projectPath)) {
            throw ProjectAlreadyExistsException()
        }

        Files.createDirectories(projectPath)

        val projectFile = projectPath.resolve("project.xml")

        val fileProject = FileProject();
        fileProject.name = name;
        fileProject.uuid = UUID.randomUUID().toString()

        Context.write(projectFile, fileProject)

        return map(fileProject)
    }

    private fun map(fileProject: FileProject): Project {

        val project = Project()
        project.name = fileProject.name
        return project
    }

    fun createRequest(project: Project, create: RequestCreate): Request {

        throw RuntimeException()
    }

    fun loadRequest(project: Project, id: String): Request {


        val path = resolveProjectPath(project).resolve(id)

        val mapper = ObjectMapper(YAMLFactory())
        val fileRequest = mapper.readValue(path.toFile(), FileRequest::class.java)

        val request = Request()
        request.content = fileRequest.content


        throw RuntimeException()

    }


    private fun resolveProjectPath(project: Project): Path {

        return baseDirectory.resolve(project.name);
    }
}