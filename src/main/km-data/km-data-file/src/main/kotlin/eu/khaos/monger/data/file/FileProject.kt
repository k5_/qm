package eu.khaos.monger.data.file

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.NONE)
class FileProject {

	@XmlElement
	var name: String? = null;


	@XmlElement
	var uuid: String? = null;
	
	
}