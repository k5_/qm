package eu.khaos.monger.data.file

import java.nio.file.Path
import javax.xml.bind.JAXBContext

object Context {

    private val context: JAXBContext = JAXBContext.newInstance(FileProject::class.java)


    fun open(path: Path): FileProject {
        
        var obj = context.createUnmarshaller().unmarshal(path.toFile())

        if (obj is FileProject) {
            return obj
        }
        throw IllegalArgumentException()
    }

    fun write(path: Path, fileProject: FileProject) {
        context.createMarshaller().marshal(fileProject, path.toFile())

    }
}