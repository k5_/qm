package eu.khaos.monger.wsdl

import org.junit.Assert
import org.junit.Test
import java.io.File
import javax.xml.bind.JAXBContext

class WsdlWikiExampleTest {

	@Test
	fun test() {
		val context = JAXBContext.newInstance(Wsdl::class.java);

		val unmarshaller = context.createUnmarshaller();

		val unmarshalled = unmarshaller.unmarshal(File("src/test/resources/wsdls/WikiExample.wsdl"));

		if (unmarshalled is Wsdl) {
			Assert.assertNotNull(unmarshalled.types);
			Assert.assertFalse(unmarshalled.message.isEmpty());
			Assert.assertNotNull(unmarshalled.portType);
			Assert.assertNotNull(unmarshalled.binding);
			Assert.assertFalse(unmarshalled.service.isEmpty());

			assertPortType(unmarshalled.portType!!);

		} else {
			Assert.fail("Not wasdl");
		}
	}

	fun assertPortType(port: PortType) {

		Assert.assertNotNull(port.operation);
		Assert.assertNotNull(port.operation!!.input);
		Assert.assertNotNull(port.operation!!.output);

		Assert.assertEquals("StockQuotePortType", port.name);
		Assert.assertEquals("GetLastTradePrice", port.operation!!.name);


		println(port.operation!!.input!!.message!!);


	}
}