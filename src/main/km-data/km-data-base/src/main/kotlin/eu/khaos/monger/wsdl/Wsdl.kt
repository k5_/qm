package eu.khaos.monger.wsdl

import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlAccessType
import com.sun.java.swing.plaf.windows.WindowsSliderUI

@XmlRootElement(name = "definitions", namespace = WSDL_NS)
@XmlAccessorType(XmlAccessType.NONE)
class Wsdl {


	@XmlElement(name = "types", namespace = WSDL_NS)
	var types: Types? = null

	@XmlElement(name = "message", namespace = WSDL_NS)
	var message: List<Message> = ArrayList()

	@XmlElement(name = "portType", namespace = WSDL_NS)
	var portType: PortType? = null

	@XmlElement(name = "binding", namespace = WSDL_NS)
	var binding: Binding? = null

	@XmlElement(name = "service", namespace = WSDL_NS)
    var service: List<Service> = ArrayList()
}