package eu.khaos.monger.wsdl

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement

@XmlAccessorType(XmlAccessType.NONE)
class PortType {
	
	@XmlAttribute
	val name : String? = null
	@XmlElement
	val operation: Operation? = null

	@XmlAccessorType(XmlAccessType.NONE)
	class Operation {
		@XmlAttribute
		var name: String? = null

		@XmlElement
		var input: OperationMessage? = null

		@XmlElement
		var output: OperationMessage? = null


		@XmlAccessorType(XmlAccessType.NONE)
		class OperationMessage {
			@XmlAttribute
			var message: String? = null
		}
	}
}