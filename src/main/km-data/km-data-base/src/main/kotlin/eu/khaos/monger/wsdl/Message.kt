package eu.khaos.monger.wsdl

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement



@XmlAccessorType(XmlAccessType.NONE)
class Message {

	@XmlAttribute(name = "name")
	var name: String? = null

	@XmlElement(name = "part")
	var parts: List<MessagePart>? = null

	@XmlAccessorType(XmlAccessType.NONE)
	class MessagePart {

		@XmlAttribute
		var name: String? = null

		@XmlAttribute
		var element : String? = null

	}
}