package eu.khaos.monger.wsdl

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement



@XmlAccessorType(XmlAccessType.NONE)
class Service {

	@XmlAttribute
	val name: String? = null

	@XmlElement
	val port:Port? = null

	@XmlAccessorType(XmlAccessType.NONE)
	class Port {

		@XmlAttribute
		val name: String? = null


		@XmlAttribute
		val bindings: String? = null
	}
}