package eu.khaos.monger.res

import javax.ws.rs.core.Application

class App : Application() {

	override fun getClasses(): Set<Class<*>>? {

		val classes = HashSet<Class<*>>()
		classes.add(DataResource::class.java);
		return classes;
	}
}
