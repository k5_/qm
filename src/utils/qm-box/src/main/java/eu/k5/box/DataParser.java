package eu.k5.box;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.box.model.Block;
import eu.k5.box.model.BlockHeader;
import eu.k5.box.model.Meta;

public class DataParser {
	private static final String SECTION_PREFIX = ">>>";

	private enum State {
		START("", "") {

			@Override
			public void setContent(DataBuilder data, RawMetaBuilder content) {
				// TODO Auto-generated method stub

			}
		},
		HEADER("Header", "HeaderEnd") {

			@Override
			public void setContent(DataBuilder data, RawMetaBuilder content) {
				data.setHeaderContent(content);
			}

		},
		META("Meta", "MetaEnd") {
			@Override
			public void setContent(DataBuilder data, RawMetaBuilder content) {
				data.addMeta(content);
			}
		},
		CONTENT("Content", "ContentEnd") {
			@Override
			public void setContent(DataBuilder data, RawMetaBuilder content) {
				data.addData(content);
			}
		};
		private String start;
		private String end;

		private State(String start, String end) {
			this.start = SECTION_PREFIX + start;
			this.end = SECTION_PREFIX + end;
		}

		public abstract void setContent(DataBuilder data, RawMetaBuilder content);
	}

	private static class DataBuilder {
		private RawMetaBuilder headerContent;
		private List<RawMetaBuilder> metaContent = new ArrayList<>();
		private List<RawContentBuilder> dataContent = new ArrayList<>();

		public void setHeaderContent(RawMetaBuilder headerContent) {
			this.headerContent = headerContent;
		}

		public void addData(RawContentBuilder content) {
			dataContent.add(content);
		}

		public void addMeta(RawMetaBuilder content) {
			metaContent.add(content);
		}

		public Block build() {
			try {
				BlockHeader header = new ObjectMapper().readValue(headerContent.content.toString(), BlockHeader.class);

				LinkedHashMap<String, Meta> metas = new LinkedHashMap<>();

				for (RawMetaBuilder rawMeta : this.metaContent) {
					Meta meta = rawMeta.buildMeta();
					metas.put(meta.getHeader().getName(), meta);
				}

				for (RawContentBuilder raw : this.dataContent) {
raw.build();
				}

				return new Block(header, metas);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
	}

	public Block parse(Reader reader) {

		DataBuilder builder = new DataBuilder();
AtomicInteger
		State state = State.START;

		RawMetaBuilder content = new RawMetaBuilder();
		boolean meta = false;
		try (BufferedReader br = new BufferedReader(reader)) {
			String line;
			while ((line = br.readLine()) != null) {

				if (state.end.equals(line.trim())) {
					state.setContent(builder, content);
					content = new RawMetaBuilder();
				} else {
					if (meta && line.startsWith(">>>")) {
						content.appendMeta(line.substring(3));

					} else {
						meta = false;
						content.appendContent(line);
					}
				}
				if ((SECTION_PREFIX + "Header").equals(line.trim())) {
					state = State.HEADER;
					meta = true;
					content = new RawMetaBuilder();
				} else if (State.META.start.equals(line.trim())) {
					state = State.META;
					meta = true;
					content = new RawMetaBuilder();
				} else if (State.CONTENT.start.equals(line.trim())) {
					state = State.CONTENT;
					meta = true;
					content = new RawMetaBuilder();
				}
			}

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return builder.build();
	}

}
