package eu.k5.box;

public class LazySIngelton {

	public static class Holder {
		static final LazySIngelton INSANCE = new LazySIngelton();
	}

	public static LazySIngelton getInstance() {
		return Holder.INSANCE;
	}
}
