package eu.k5.box;

public interface DataTypeBuilder {

	String getType();
}
