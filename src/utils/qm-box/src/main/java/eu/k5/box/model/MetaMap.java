package eu.k5.box.model;

import java.util.Map;

public class MetaMap extends Meta {

	private final Map<String, String> map;

	public MetaMap(MetaHeader header, Map<String, String> map) {
		super(header);
		this.map = map;
	}

	public String getValue(String key){
		return map.get(key);
	}
	
}
