package eu.k5.box;

public class Test {
	static class X {
		static void printout() {
			System.out.println("hello");
		}
	}
	static class Y extends X{
		static void printout() {
			System.out.println("helloY");
		}
	}
	public static void main(String[] args) {
		X x = null;
		x.printout();
		x = new Y();
		x.printout();
		
		
	}
}
