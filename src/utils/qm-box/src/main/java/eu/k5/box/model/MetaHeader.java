package eu.k5.box.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class MetaHeader {
	private String name;
	private String type;

	@JsonIgnore
	private String raw;

	@JsonIgnore
	private int index;

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getRaw() {
		return raw;
	}

	public void setRaw(String raw) {
		this.raw = raw;
	}
}
