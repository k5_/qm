package eu.k5.box.model;


public class Meta {

	private final MetaHeader header;

	public Meta(MetaHeader header) {
		this.header = header;
	}
	
	public MetaHeader getHeader() {
		return header;
	}
	

}
