package eu.k5.box.parser;

import eu.k5.box.model.Meta;
import eu.k5.box.model.MetaHeader;

public interface MetaParser {

	String getType();

	Meta build(MetaHeader headers, String string);

}
