package eu.k5.box;

class Visitor {
	void call(A a) {
		System.out.println("a");

	}

	void call(B a) {
		System.out.println("b");

	}

}

class A {
	void accept(Visitor visitor) {
		visitor.call(this);
	}
}

class B extends A {
	@Override
	void accept(Visitor visitor) {
		visitor.call(this);
	}
}

public class Test2 {
	public static void main(String[] args) {
		A a = new B();

		a.accept(new Visitor());
	}

}
