package eu.k5.box;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

import eu.k5.box.parser.ContentParser;
import eu.k5.box.parser.MetaParser;

public enum ParserRegistry {
	INSTANCE;

	private final Map<String, ContentParser> dataBuilders;
	
	private final Map<String, MetaParser> metaBuilders;
	

	private ParserRegistry() {
		ServiceLoader<ContentParser> types = ServiceLoader.load(ContentParser.class);
	
		Map<String, ContentParser> dataBuilders = new HashMap<>();
		for(ContentParser builder:types) {
			dataBuilders.put(builder.getType(),builder);
		}
	
		ServiceLoader<MetaParser> metaTypes = ServiceLoader.load(MetaParser.class);
		
		Map<String, MetaParser> metaBuilders = new HashMap<>();
		for(MetaParser builder:metaTypes) {
			metaBuilders.put(builder.getType().toLowerCase(),builder);
		}
	
		
		this.dataBuilders = Collections.unmodifiableMap(dataBuilders);
		this.metaBuilders = Collections.unmodifiableMap(metaBuilders);

	}

	public static ParserRegistry getInstance() {
		return INSTANCE;
	}
	
	public static ContentParser forType(String typeName) {
		return getInstance().dataBuilders.get(typeName);
	}

	public static MetaParser forMeta(String typeName) {
		return getInstance().metaBuilders.get(typeName.toLowerCase());
	}

	public static ContentParser forContent(String type) {
		return getInstance().dataBuilders.get(type);
	}

}
