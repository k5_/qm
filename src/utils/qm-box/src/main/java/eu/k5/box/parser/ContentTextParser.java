package eu.k5.box.parser;

import java.nio.charset.StandardCharsets;

import eu.k5.box.model.Content;
import eu.k5.box.model.ContentHeader;

public class ContentTextParser implements ContentParser {

	@Override
	public Content build(ContentHeader headers, String string) {

		byte[] bytes = string.getBytes(StandardCharsets.UTF_8);

		return new Content(headers, bytes);
	}

	@Override
	public String getType() {
		return "text";
	}

}
