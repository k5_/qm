package eu.k5.box.parser;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.box.model.MetaHeader;
import eu.k5.box.model.MetaMap;

public class MetaMapParser implements MetaParser {

	@Override
	public String getType() {
		return "map";
	}

	@Override
	public MetaMap build(MetaHeader header, String content) {

		try {
			Map<String, String> map = new ObjectMapper().readValue(content, new TypeReference<Map<String, String>>() {
			});

			return new MetaMap(header, map);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

}
