package eu.k5.box.model;

import java.util.LinkedHashMap;
import java.util.Map;


public class Block {
	private final BlockHeader header;

	private LinkedHashMap<String, Meta> metas;

	private Map<String, Content> contents;

	public Block(BlockHeader header, LinkedHashMap<String, Meta> metas) {
		this.header = header;
		this.metas = metas;
	}

	public BlockHeader getHeader() {
		return header;
	}

	public <M> M getMeta(String name, Class<M> type) {
		Meta obj = metas.get(name);
		if (type.isInstance(obj)) {
			return type.cast(obj);
		}
		return null;
	}

	public Content getContent(String name) {

		return contents.get(name);
	}
}
