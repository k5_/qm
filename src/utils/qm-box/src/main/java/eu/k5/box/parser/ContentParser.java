package eu.k5.box.parser;

import eu.k5.box.model.Content;
import eu.k5.box.model.ContentHeader;

public interface ContentParser {

	Content build(ContentHeader headers, String string);
	
	String getType();

}
