package eu.k5.box.model;

public class ContentHeader {
	private String name;
	private String raw;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRaw(String raw) {
		this.raw = raw;
		
	}

	public String getType() {
		return "text";
	}

}
