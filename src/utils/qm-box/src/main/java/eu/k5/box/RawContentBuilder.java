package eu.k5.box;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.box.model.Content;
import eu.k5.box.model.ContentHeader;
import eu.k5.box.parser.ContentParser;
import eu.k5.box.parser.MetaParser;

public class RawContentBuilder {
	private ContentHeader headers;
	StringBuilder content = new StringBuilder();

	public void appendMeta(String newMeta) {
		try {
			ContentHeader value = new ObjectMapper().readValue(newMeta, ContentHeader.class);
			value.setRaw(newMeta);
			headers = value;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void appendContent(String newContent) {
		content.append(newContent).append('\n');
	}

	public Content build() {
		ContentParser meta = ParserRegistry.forContent(headers.getType());

		return meta.build(headers, content.toString());
	}

}
