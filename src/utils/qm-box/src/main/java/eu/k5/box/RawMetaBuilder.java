package eu.k5.box;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.k5.box.model.Meta;
import eu.k5.box.model.MetaHeader;
import eu.k5.box.parser.MetaParser;

public class RawMetaBuilder {
	private MetaHeader headers;
	StringBuilder content = new StringBuilder();

	public void appendMeta(String newMeta) {
		try {
			MetaHeader value = new ObjectMapper().readValue(newMeta, MetaHeader.class);
			value.setRaw(newMeta);
			headers = value;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void appendContent(String newContent) {
		content.append(newContent).append('\n');
	}

	public Meta buildMeta() {
		MetaParser meta = ParserRegistry.forMeta(headers.getType());

		return meta.build(headers, content.toString());
	}

}