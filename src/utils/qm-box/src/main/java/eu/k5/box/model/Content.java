package eu.k5.box.model;

public class Content {

	private final ContentHeader header;

	private final byte[] bytes;

	public Content(ContentHeader header, byte[] bytes) {
		this.header = header;
		this.bytes = bytes;
	}

	public ContentHeader getHeader() {
		return header;
	}

	public byte[] getBytes() {
		return bytes;
	}

}
