abstract class BaseClass {

	abstract int methodDecorator(int originalReturn);

	int method(int arg) {
		// do 99% of the work
		return methodDecorator(0);
	} 
}

interface MixinInterface {
	default int methodDecorator(int arg) {
		return arg;
	}
}

class Subclass extends BaseClass implements MixinInterface {
	@Override
	public int methodDecorator(int originalReturn) {
		return MixinInterface.super.methodDecorator(originalReturn);
	}
}