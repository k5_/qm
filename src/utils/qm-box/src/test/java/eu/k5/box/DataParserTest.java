package eu.k5.box;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import org.junit.Assert;
import org.junit.Test;

import eu.k5.box.model.Block;
import eu.k5.box.model.Content;
import eu.k5.box.model.MetaMap;

public class DataParserTest {

	@Test
	public void headerOnly() {
		Block data = parse("headeronly");
		Assert.assertNotNull(data);
		Assert.assertNotNull(data.getHeader());
		Assert.assertEquals("0.1", data.getHeader().getVersion());
		Assert.assertEquals("WsdlRequest", data.getHeader().getType());
	}

	@Test
	public void headerandmeta() {
		Block data = parse("headerandmeta");
		assertNotNull(data);
		
		MetaMap meta = data.getMeta("properties", MetaMap.class);
		String op = meta.getValue("operation");
		assertEquals("SetAbc", op);
		
	}

	@Test
	public void content(){
		Block block = parse("textcontent");
		assertNotNull(block);
		
		Content content = block.getContent("request");
		assertNotNull(content);
		
		String textContent = new String(content.getBytes(), StandardCharsets.UTF_8);
		assertEquals("<wsdl></wsdl>", textContent);
	}
	
	private Block parse(String name) {
		try (Reader reader = openData(name)) {
			return new DataParser().parse(reader);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Reader openData(String name) {
		InputStream is = DataParserTest.class.getResourceAsStream("/example/" + name + ".data.wss");
		return new InputStreamReader(is, StandardCharsets.UTF_8);
	}
}
