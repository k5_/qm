package eu.k5.tolerantreader.xs

import java.nio.file.Path
import java.nio.file.Paths
import javax.xml.bind.JAXBContext

object Schema {
    val context = JAXBContext.newInstance(XsSchema::class.java)

    fun parse(location: Path): XsRegistry {
        val fileName = location.toAbsolutePath().normalize().toString()

        val init = read(fileName)
        val all = HashMap<String, XsSchema>()
        all.put(fileName, init)
        readRecursive(init, all)

        return XsRegistry(init, all)
    }

    private fun readRecursive(current: XsSchema, resolved: MutableMap<String, XsSchema>) {

        for (im in current.imports) {

            val filename = getFilename(current, im)

            val imported: XsSchema
            if (resolved.containsKey(filename)) {
                imported = resolved[filename]!!
            } else {
                imported = read(filename)
                resolved.put(filename, imported)
                readRecursive(imported, resolved)
            }
            im.resolvedSchema = imported
        }
    }

    private fun getFilename(current: XsSchema, read: XsImport): String {
        val currentLocation = current.schemaLocation

        val resolvedPath = Paths.get(currentLocation).parent.resolve(read.schemaLocation)
        return resolvedPath.toAbsolutePath().normalize().toString()

    }

    private fun read(filename: String): XsSchema {

        val resolvedPath = Paths.get(filename)

        val obj = context.createUnmarshaller().unmarshal(resolvedPath.toFile())
        if (obj is XsSchema) {
            obj.schemaLocation = filename
            obj.complete()
            return obj
        } else {
            throw RuntimeException("Invalid file content, expected xsSchema")
        }
    }

}
