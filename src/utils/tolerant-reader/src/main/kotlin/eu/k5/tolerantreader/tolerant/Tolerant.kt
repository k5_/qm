package eu.k5.tolerantreader.tolerant

import javax.xml.namespace.QName

val XSI_TYPE = QName("http://www.w3.org/2001/XMLSchema-instance", "type")
const val NAMESPACE_SEPARATOR = ':'