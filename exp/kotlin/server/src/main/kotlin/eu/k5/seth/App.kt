package eu.k5.seth

import eu.k5.seth.resources.Projects
import javax.ws.rs.ApplicationPath
import javax.ws.rs.core.Application

@ApplicationPath("/rest/*")
class App : Application() {

	override fun getClasses() : Set<Class<*>>? {
		
		val classes = HashSet<Class<*>>()
		classes.add(Projects::class.java);
		return classes;
	}
}
