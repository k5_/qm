package eu.k5.exp



import javafx.application.Application

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.layout.StackPane
import javafx.stage.Stage

fun main(args: Array<String>) {
    Application.launch(World::class.java);
}

class World : Application() {
    override fun start(stage: Stage?) {
        stage!!.title = "Hello World"

        val button = Button()
        button.text = "Say Hello"
        button.onAction = EventHandler<ActionEvent> { println("Hello world") }

        val root = StackPane()
        root.children.add(button)

        stage.scene = Scene(root, 300.0, 300.0)
        stage.show()
        println("Hello world")


    }
}
