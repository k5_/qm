package eu.k5.qm;

import java.util.List;

public class Section {

	private List<String> rawMeta;

	private String raw;

	public Section(String raw, List<String> rawMeta) {
		this.rawMeta = rawMeta;
		this.raw = raw;
	}

}