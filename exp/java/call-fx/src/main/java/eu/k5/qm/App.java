package eu.k5.qm;

import eu.k5.qm.request.RequestView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class App extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Hello fx");

		RequestView requestView = new RequestView();
		
		
		StackPane pane = new StackPane();
		pane.getChildren().add(requestView.asNode());

		primaryStage.setScene(new Scene(pane, 300, 300));
		primaryStage.show();
	}
}
