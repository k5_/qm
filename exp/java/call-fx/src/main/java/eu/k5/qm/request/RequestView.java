package eu.k5.qm.request;


import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class RequestView {

	private Button start = new Button("Start");
	
	private Button stop = new Button("Stop");
	
	private ComboBox<Endpoint> endpoints = new ComboBox<>();
	
	private TextArea request = new TextArea();
	
	private TextArea response = new TextArea();
	
	
	public VBox asNode() {
		VBox vBox = new VBox();
		
		
		HBox controlBar = new HBox();
		controlBar.getChildren().addAll(start, stop, endpoints);
		
		
		SplitPane split = new SplitPane();
		split.getItems().add(request);
		split.getItems().add(response);
		
		
		vBox.getChildren().addAll(controlBar, split);
		
		return vBox;
	}
	
	
}
