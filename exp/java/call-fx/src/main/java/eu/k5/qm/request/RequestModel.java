package eu.k5.qm.request;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;

public class RequestModel {
	private Property<String> request = new SimpleStringProperty();
	
	public Property<String> request() {
		return request;
	}
}
